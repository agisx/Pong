﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallControl : MonoBehaviour
{

    // Rigidbody 2D raket ini
    private Rigidbody2D rgb2d;

    //besar gaya awal
    public float xInitialForce;
    public float yInitialForce;

    // Titik asal lintasan bola saat ini
    public Vector2 trajectoryOrigin { get; private set; }

    // Start is called before the first frame update
    void Start(){
        rgb2d = GetComponent<Rigidbody2D>();
        RestartGame();
        trajectoryOrigin = transform.position;
    }

    private void RestartGame() {
        ResetBall();
        Invoke("PushBall", 2);
    }

    private void OnCollisionExit2D(Collision2D collision) {
        // Ketika bola beranjak dari sebuah tumbukan,
        // rekam titik tumbukan tersebut
        trajectoryOrigin = transform.position;
    }

    private void PushBall() { 

        float randomDirection = Random.Range(0, 2);

        Vector3 direction; 
        if (randomDirection < 1.0f) {
            direction = new Vector2(-xInitialForce, yInitialForce); 
        } else {
            direction = new Vector2(xInitialForce, yInitialForce);
        }
        rgb2d.AddForce(direction);
    }
    void ResetBall() { //reset position and velocity the ball
        transform.position = Vector2.zero;

        rgb2d.velocity = Vector2.zero;
    }
}
