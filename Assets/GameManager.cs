﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    //player1
    public PlayerControl player1;
    private Rigidbody2D player1Rigidbody ;

    //player2
    public PlayerControl player2;
    private Rigidbody2D player2Rigidbody ;

    //bola
    public BallControl ball;
    private Rigidbody2D ballRigidbody;
    private CircleCollider2D ballCollider;

    //skor maksimal
    public int maxScore;

    public static GameManager Instance { get; private set; }

    //Toggle Debug Window
    private bool isDebugWindowShown = false;

    //Objek untuk menggambar prediksi lintasan bola
    public Trajectory trajectory;

    private void Awake() {
        if (Instance == null) {
            Instance = this;
        }
    }
    void Start(){
        player1Rigidbody = player1.GetComponent<Rigidbody2D>();

        player2Rigidbody = player2.GetComponent<Rigidbody2D>();

        ballRigidbody = ball.GetComponent<Rigidbody2D>();
        ballCollider = ball.GetComponent<CircleCollider2D>();
    } 
    private void OnGUI() {
        if (isDebugWindowShown) {
            Color oldColor = GUI.backgroundColor;

            GUI.backgroundColor = Color.red;

            GUIStyle guiStyle = new GUIStyle(GUI.skin.textArea);
            guiStyle.alignment = TextAnchor.UpperCenter;
            GUI.TextArea(new Rect(Screen.width / 2 - 200, Screen.height - 200, 400, 110), DebugWindowShown(), guiStyle);
            GUI.backgroundColor = oldColor;
        }

        if (GUI.Button(new Rect(Screen.width / 2 - 60, Screen.height - 73, 120, 53), "TOGGLE\nDEBUG INFO")){
            isDebugWindowShown = !isDebugWindowShown;
            trajectory.enabled = !trajectory.enabled;
        }

        //tampilkan score player 1 kiri atas dan 
        //player 2 kanan atas
        GUI.Label(new Rect(Screen.width / 2 - 150 - 12,20, 100, 100), "" + player1.score);    //text
        GUI.Label(new Rect(Screen.width / 2 + 150 - 12,20, 100, 100), "" + player2.score);    //text

        if (GUI.Button(new Rect(Screen.width / 2 - 60, 35, 120, 53), "RESTART")) {
            player1.ResetScore();
            player2.ResetScore();
            ball.SendMessage("RestartGame", 0.5f, SendMessageOptions.RequireReceiver);
        }

        if (player1.score == maxScore) {
            //print player 1 menang
            GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 10, 2000, 1000), "PLAYER ONE WINS");

            ball.SendMessage("ResetBall", null, SendMessageOptions.RequireReceiver);
        }else if (player2.score == maxScore) {
            //print player 2 menang
            GUI.Label(new Rect(Screen.width / 2 + 30, Screen.height / 2 - 10, 2000, 1000), "PLAYER TWO WINS");

            ball.SendMessage("ResetBall", null, SendMessageOptions.RequireReceiver);
        }
    }

    private string DebugWindowShown() {
        float ballMass = ballRigidbody.mass;
        Vector2 ballVelocity = ballRigidbody.velocity; //ektor yang mendeskripsikan perubahan posisi objek tersebut terhadap waktu
        float ballSpeed = ballRigidbody.velocity.magnitude; //besarnya (magnitude) kecepatan
        Vector2 ballMomentum = ballMass * ballVelocity; //seberapa susahnya menghentikan sebuah objek dari gerakan semulanya 
        float ballFriction = ballCollider.friction; //gaya gesek

        //total gaya yang diterapkan kepada sebuah objek selama rentang waktu tertentu
        float impulsePlayer1_X = player1.lastContactPoint.normalImpulse;
        float impulsePlayer1_Y = player1.lastContactPoint.tangentImpulse;
        float impulsePlayer2_X = player2.lastContactPoint.normalImpulse;
        float impulsePlayer2_Y = player2.lastContactPoint.tangentImpulse;

        string debugText =
                "Ball mass = " + ballMass + "\n" +
                "Ball velocity = " + ballVelocity + "\n" +
                "Ball speed = " + ballSpeed + "\n" +
                "Ball momentum = " + ballMomentum + "\n" +
                "Ball friction = " + ballFriction + "\n" +
                "Last impulse from player 1 = (" + impulsePlayer1_X + ", " + impulsePlayer1_Y + ")\n" +
                "Last impulse from player 2 = (" + impulsePlayer2_X + ", " + impulsePlayer2_Y + ")\n";
        return debugText;
    }
}
