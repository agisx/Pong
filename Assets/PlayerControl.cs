﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour{
    // Tombol untuk menggerakkan ke atas
    public KeyCode upButton = KeyCode.W;

    // Tombol untuk menggerakkan ke bawah
    public KeyCode downButton = KeyCode.S;

    // Batas atas dan bawah game scene (Batas bawah menggunakan minus (-))
    public float speed = 10.0f;
     
    // Skor pemain dengan set harus private
    public int score { get; private set; }

    // Batas atas dan bawah game scene (bawah dengan (-))
    public float yBoundary;

    // Rigidbody 2D raket ini
    private Rigidbody2D rgb2d;

    // Titik tumbukan terakhir dengan bola,
    // untuk menampilkan variabel-variabel
    // fisika terkait tumbukan tersebut
    public ContactPoint2D lastContactPoint { get; private set; }

    void Start(){
        rgb2d = GetComponent<Rigidbody2D>();
        yBoundary = Camera.main.orthographicSize - 1;
    }

    // Update is called once per frame
    void Update(){
        Vector2 currentVelocity = rgb2d.velocity;
                
        if (Input.GetKey(upButton)) { // tombol ke atas
            currentVelocity.y = speed;
        } else if (Input.GetKey(downButton)) { // tombol ke bawah
            currentVelocity.y = -speed;
        } else { // player posisi idle
            currentVelocity.y = 0.0f;
        }
        rgb2d.velocity = currentVelocity;

        // Dapatkan posisi raket sekarang.
        Vector3 currentPosition = transform.position;

        // Jika posisi raket melewati batas atas (yBoundary), kembalikan ke batas atas tersebut.
        if (currentPosition.y > yBoundary) {
            currentPosition.y = yBoundary;
        }

        // Jika posisi raket melewati batas bawah (-yBoundary), kembalikan ke batas bawah tersebut.
        else if (currentPosition.y < -yBoundary) {
            currentPosition.y = -yBoundary;
        }

        // Set posisinya ke transform.
        transform.position = currentPosition;
    }
    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Ball")) {
            lastContactPoint = collision.GetContact(0);
        }    
    } 

    public void IncrementScore() {
        score++;
    }
    
    public void ResetScore() {
        score = 0;
    }
}
