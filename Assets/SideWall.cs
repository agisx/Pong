﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideWall : MonoBehaviour{
    public PlayerControl player;
    [SerializeField] private GameManager gameManager;
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("Ball")) {
            player.IncrementScore();
            if(player.score < gameManager.maxScore) {
                collision.gameObject.SendMessage("RestartGame", 
                    2f, SendMessageOptions.RequireReceiver);
            }
        }
    }
}
